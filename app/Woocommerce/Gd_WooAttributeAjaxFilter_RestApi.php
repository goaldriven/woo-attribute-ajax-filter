<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 08/04/2019
 * Time: 10:39 PM
 */

class Gd_WooAttributeAjaxFilter_RestApi extends WP_REST_Controller {

	public function __construct() {
		add_action('rest_api_init', array($this, 'register_routes'), 99);
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$version = '1';
		$namespace = 'goaldriven';
		$base = 'woo-attribute-ajax-filter/v'. $version;

		register_rest_route( $namespace, '/'. $base .'/next-options', array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_next_options' ),
				'permission_callback' => array( $this, 'get_next_options_check' ),
				'args'                => array(

				),
			)
		) );
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_next_options( WP_REST_Request $request ) {
		$active_tax = $request->get_param('active_tax');
		$next_tax = $request->get_param('next_tax');

		// $tax_keys = array_keys($active_tax);
		$tax_values = array_values($active_tax);

		/*
		var_dump($tax_keys);
		var_dump($tax_values);
		exit;
		*/
		
		$get_terms_params = [
			'taxonomy' => $next_tax
		];
		
		if( count($tax_values) > 0 ) {
    		$get_terms_params['parent'] = end($tax_values);
		} else {
			$get_terms_params['parent'] = 0;
		}

		$terms = get_terms($get_terms_params);

		return new WP_REST_Response( $terms, 200 );
	}

	/**
	 * Check if a given request has access to get next options
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return bool
	 */
	public function get_next_options_check( WP_REST_Request $request ) {
		return true;
	}
}