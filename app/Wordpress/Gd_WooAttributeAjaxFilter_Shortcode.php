<?php defined('ABSPATH') or die(-1);

class Gd_WooAttributeAjaxFilter_Shortcode{
	public function __construct() {
		add_shortcode( 'woo-attr-ajax-filter', array($this, 'wooAttrAjaxFilter') );
	}

	function wooAttrAjaxFilter( $atts ) {
		$wooAttrAjaxFilter = shortcode_atts( array(
			'product-attrs' => []
		), $atts );

		$key_pairs = explode('|', $wooAttrAjaxFilter['product-attrs']);

		$wooAttrAjaxFilter['product-attrs'] = [];

		foreach( $key_pairs as $key_pair ) {
			$pair = explode(':', $key_pair);

			$wooAttrAjaxFilter['product-attrs'][] = [
				'taxonomy' => $pair[0],
				'label' => $pair[1]
			];
		}

		ob_start();

		include GDWAAF_DIR . '/templates/shortcode-woo-attr-ajax-filter.php';

		return ob_get_clean();
	}
}