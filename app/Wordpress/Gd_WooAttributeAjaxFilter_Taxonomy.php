<?php
/**
 * Created by PhpStorm.
 * User: IhsanBerahim
 * Date: 15/4/2019
 * Time: 11:15 PM
 */

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Gd_WooAttributeAjaxFilter_Taxonomy {
	function __construct() {
		add_action('init', array($this, 'addAutoFilterTaxonomy'));

		add_action( 'after_setup_theme', array($this, 'crb_load') );
		add_action( 'carbon_fields_register_fields', array($this, 'addImageField') );
	}

	// Register Custom Taxonomy
	function addAutoFilterTaxonomy() {

		$labels = array(
			'name'                       => _x( 'Auto Filters', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Auto Filter', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Auto Filter', 'text_domain' ),
			'all_items'                  => __( 'All Auto Filters', 'text_domain' ),
			'parent_item'                => __( 'Parent Auto Filter', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent Auto Filter:', 'text_domain' ),
			'new_item_name'              => __( 'New Auto Filter', 'text_domain' ),
			'add_new_item'               => __( 'Add Auto Filter', 'text_domain' ),
			'edit_item'                  => __( 'Edit Auto Filter', 'text_domain' ),
			'update_item'                => __( 'Update Auto Filter', 'text_domain' ),
			'view_item'                  => __( 'View Auto Filter', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove auto filter', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
			'popular_items'              => __( 'Popular Auto Filters', 'text_domain' ),
			'search_items'               => __( 'Search Auto Filters', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
			'no_terms'                   => __( 'No auto filters', 'text_domain' ),
			'items_list'                 => __( 'Auto filter list', 'text_domain' ),
			'items_list_navigation'      => __( 'Auto filter list navigation', 'text_domain' ),
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'update_count_callback'      => array($this, 'onAutoFilterCountUpdated'),
			'show_in_rest'               => true,
			'query_var'                  => true,
			'rewrite'                    => array('slug' => '', 'with_front' => false)
		);

		register_taxonomy( 'auto_filter', array( 'product' ), $args );
		register_taxonomy_for_object_type( 'auto_filter', 'product' );
	}

	function onAutoFilterCountUpdated() {
		//
	}

	function addImageField() {
		Container::make( 'term_meta', __( 'Extra Field' ) )
		         ->where( 'term_taxonomy', '=', 'auto_filter' )
		         ->add_fields( array(
			         Field::make( 'image', 'crb_photo', __( 'Photo' ) ),
		         ) );
	}

	function crb_load() {
		Carbon_Fields::boot();
	}
}