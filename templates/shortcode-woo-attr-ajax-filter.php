<?php defined('ABSPATH') or die(-1);
/**
 * @var array $wooAttrAjaxFilter = [ 'product-attrs' => [] ]
 */
?>
	<!--START: gd_shortcode_woo_attr_ajax_filter-->
	<div id="gd_shortcode_woo_attr_ajax_filter"></div>
	<!--END: gd_shortcode_woo_attr_ajax_filter-->
	<script>
    <?= waaf_comp()->requirecss('woo_attr_ajax_filter', '/assets/components/woo-attribute-ajax-filter/index.css')
    ?>
	<?= waaf_comp()->wp_require_object('wooAttributeAjaxFilter', $wooAttrAjaxFilter['product-attrs'])
	?>
<?= waaf_comp()->requirejs('Vue', '/node_modules/vue/dist/vue.min.js',
    waaf_comp()->requirejs('VueI18n', '/node_modules/vue-i18n/dist/vue-i18n.min.js',
	waaf_comp()->requirejs('axios', '/node_modules/axios/dist/axios.min.js',
	waaf_comp()->requirejs('_', '/node_modules/underscore/underscore-min.js',
	waaf_comp()->requirejs('jQuery', '/node_modules/jquery/dist/jquery.min.js',
		waaf_comp()->requirejs(false, '/assets/components/woo-attribute-ajax-filter/index.js')
    )))));
?>
</script>
