new Vue({
    el: '#gd_shortcode_woo_attr_ajax_filter',
    template: "<div v-bind:class=\"[columnClass]\" class=\"prdctfltr_filter_wrapper prdctfltr_sc prdctfltr_sc_filter woocommerce  fami-prdctfltr-product-filter prdctfltr_sc_step_filter\" v-bind:data-columns=\"step_tax.length\">\n            <div class=\"prdctfltr_filter_inner prdctfltr_wc prdctfltr_woocommerce woocommerce prdctfltr_wc_regular pf_default prdctfltr_always_visible prdctfltr_click prdctfltr_rows prdctfltr_scroll_default pf_mod_multirow pf_adptv_default prdctfltr_round prdctfltr_hierarchy_circle prdctfltr_adoptive_reorder prdctfltr_selected_reorder prdctfltr_step_filter pf_remove_clearall\">\n                <form name=\"gd_shortcode_woo_attr_ajax_filter\" class=\"prdctfltr_woocommerce_ordering\">\n                    <div class=\"prdctfltr_filter prdctfltr_attributes prdctfltr_single prdctfltr_terms_customized prdctfltr_terms_customized_select wc_settings_prdctfltr_term_customization_5c8611ba57be9\" \n                        v-bind:data-filter=\"step.taxonomy\" v-for=\"(step, step_index) in step_tax\">\n                        <span class=\"prdctfltr_regular_title\" v-if=\"!param_labels[step_index]\" v-bind:data-org_title=\"step.label\" v-on:click.prevent=\"toggleOption($event, step)\"><div v-html=\"step.label\"></div>\n                            <i class=\"prdctfltr-down\" v-if=\"options[step_index]\" v-bind:style=\"styles.noClick\"></i>\n                        </span>\t\t\t\n                        <span class=\"prdctfltr_regular_title\" v-if=\"param_labels[step_index]\" v-bind:data-org_title=\"step.label\" v-on:click.prevent=\"toggleOption($event, step)\"><div v-html=\"param_labels[step_index]\"></div>\n                            <i class=\"prdctfltr-up\" v-if=\"options[step_index]\" v-bind:style=\"styles.noClick\"></i>\n                        </span>\t\t\t\n                        <div class=\"prdctfltr_add_scroll prdctfltr_down\" style=\"display: none;\">\n                            <div class=\"prdctfltr_checkboxes\" v-if=\"options[step_index]\">\n                                <label class=\"prdctfltr_ft_--\" v-for=\"(option, index) in options[step_index]\">\n                                    <input type=\"checkbox\" v-bind:name=\"'active_tax[' + option.taxonomy + ']'\" \n                                        v-on:change=\"selectOption($event, step, option)\"\n                                        v-bind:value=\"option.term_id\" v-bind:checked=\"woo_params[step_index] === option.slug\">\n                                    <span>\n                                        <span class=\"prdctfltr_customize_select prdctfltr_customize\">\n                                            <span class=\"prdctfltr_customize_name\">{{ option.name }}</span>\n                                        </span>\n                                    </span>\n                                </label>\t\t\t\t\t\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"prdctfltr_buttons\">\n                        <a class=\"button prdctfltr_woocommerce_filter_submit pf_stopajax\" href=\"#\" v-on:click.prevent=\"submit($event)\">{{ isLoading ? 'Loading...' : 'Go' }}\n                        </a>\n                    </div>\n                    <input type=\"hidden\" name=\"next_tax\" v-model=\"params.next_tax\">\n                </form>\n                <form method=\"GET\" name=\"gd_shortcode_woo_attr_ajax_form\">\n                    <input type=\"hidden\" v-for=\"(value, step_index) in woo_params\" v-bind:name=\"step_tax[step_index].taxonomy\" v-bind:value=\"value\"/>\n                </form>\n            </div>\n        </div>\n    ",
    data: function () {
        var _this = this;
        var step_tax = wooAttributeAjaxFilter();
        return {
            api_next_options: '/wp-json/goaldriven/woo-attribute-ajax-filter/v1/next-options',
            step_tax: step_tax,
            params: {
                active_tax: [],
                next_tax: step_tax[0].taxonomy
            },
            woo_params: [],
            param_labels: [],
            isLoading: false,
            options: _.chain(step_tax).reduce(function (existing, step) {
                var step_index = _.chain(_this.step_tax).findIndex({ label: step.label }).value();
                existing[step_index] = false;
                return existing;
            }, {}).value(),
            columnClass: 'prdctfltr_columns_' + step_tax.length,
            styles: {
                noClick: {
                    pointerEvents: 'none'
                }
            }
        };
    },
    mounted: function () {
        this.next(0);
    },
    methods: {
        next: function (step_index) {
            var _this = this;
            var filter = this.getFilter();
            var query = jQuery(this.getFilter()).serialize();
            this.clearOption(step_index);
            this.isLoading = true;
            // load option for first tax. step
            axios.get(this.api_next_options + '?' + unescape(query)).then(function (res) {
                // Store options
                Vue.set(_this.options, step_index, res.data);
                Vue.set(_this.params, 'next_tax', (_this.step_tax[step_index + 1] || {}).taxonomy);
                _this.isLoading = false;
            });
        },
        submit: function ($event) {
            this.getForm().submit();
        },
        selectOption: function ($event, step, option) {
            var checkbox = $event.currentTarget;
            //close option
            var optionElement = checkbox.parentElement.parentElement.parentElement;
            jQuery(optionElement).toggle();
            var step_index = this.getIndexByLabel(step.label);
            if (checkbox.checked) {
                Vue.set(this.woo_params, step_index, option.slug);
                Vue.set(this.param_labels, step_index, option.name);
                var next_index = step_index + 1;
                this.next(next_index);
                this.deleteSelected(next_index);
            }
            else {
                this.deleteSelected(step_index);
            }
        },
        toggleOption: function ($event, step) {
            var step_index = this.getIndexByLabel(step.label);
            if (this.options[step_index] && this.options[step_index].length && this.options[step_index].length > 0) {
                var optionElement = $event.currentTarget.nextElementSibling;
                jQuery(optionElement).toggle();
            }
        },
        getForm: function () {
            return document.forms['gd_shortcode_woo_attr_ajax_form'];
        },
        getFilter: function () {
            return document.forms['gd_shortcode_woo_attr_ajax_filter'];
        },
        getIndexByLabel: function (label) {
            return _.chain(this.step_tax).findIndex({ label: label }).value();
        },
        clearOption: function (step_index) {
            var _this = this;
            _.chain(this.options).forEach(function (option, index) {
                if (index > step_index) {
                    Vue.set(_this.options, step_index, false);
                }
            });
        },
        deleteSelected: function (step_index) {
            var _this = this;
            _.chain(this.woo_params).forEach(function (v, i) {
                if (i >= step_index) {
                    Vue.set(_this.woo_params, i, false);
                }
            });
            _.chain(this.param_labels).forEach(function (v, i) {
                if (i >= step_index) {
                    Vue.set(_this.param_labels, i, false);
                    var titleEl = document.querySelectorAll('.prdctfltr_regular_title')[i];
                    if (titleEl && titleEl.childNodes[0] && titleEl.childNodes[0].textContent) {
                        titleEl.childNodes[0].textContent = titleEl.getAttribute('data-org_title');
                    }
                }
            });
            this.$forceUpdate();
            this.$nextTick();
        }
    }
});
