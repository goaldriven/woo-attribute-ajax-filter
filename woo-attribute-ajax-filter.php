<?php
/**
 * Plugin Name:     Woo Attribute Ajax Filter
 * Plugin URI:      https://bitbucket.org/goaldriven/woo-attribute-ajax-filter
 * Description:     Allow to build cross filter using Woocommerce product attributes.
 * Author:          GoalDriven
 * Author URI:      https://goaldriven.co
 * Text Domain:     woo-attribute-ajax-filter
 * Domain Path:     /languages
 * Version:         1.0.5
 *
 * @package         Woo_Attribute_Ajax_Filter
 */


try {
	require_once( __DIR__ . '/vendor/autoload.php' );
} catch ( Exception $e ) {
	return;
}

define( 'GDWAAF_DIR', dirname( __FILE__ ) );

require_once( GDWAAF_DIR . '/app/Woocommerce/Gd_WooAttributeAjaxFilter_RestApi.php' );
require_once( GDWAAF_DIR . '/app/Wordpress/Gd_WooAttributeAjaxFilter_Shortcode.php' );
require_once( GDWAAF_DIR . '/app/Wordpress/Gd_WooAttributeAjaxFilter_Taxonomy.php' );

if ( ! function_exists( 'waaf_comp' ) ) {
	function waaf_comp() {
		return new \GoalDriven\Supports\Services\Component( __FILE__ );
	}
}

add_action( 'setup_theme', function () {
	if ( ! defined( 'GDWPS_ACTIVE' ) ) {
		add_action( 'admin_notices', function () {
			?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'Gd Wordpress Support required and should be activated first.' ); ?></p>
            </div>
			<?php
		} );

		return;
	}

	/**
	 * Kickstart Plugin
	 */
	new Gd_WooAttributeAjaxFilter_Taxonomy();
	new Gd_WooAttributeAjaxFilter_RestApi();
	new Gd_WooAttributeAjaxFilter_Shortcode();


	/**
	 * Setup version checking
	 */
	if( $checker = gd_setup_plugin_update_checker( __FILE__ ) ) {
	    $checker->setBranch('master');
    }
} );
